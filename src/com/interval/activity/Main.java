package com.interval.activity;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        try {
            double[][] interval = readInterval();
            System.out.println(interval[1][1]);
            //do anything with interval
        } catch (InputMismatchException e) {
            System.out.print("Please enter number in double format");
        } catch (NumberFormatException e) {
            System.out.print("End of interval must be more than begin of interval");
        }
    }

    private static double[][] readInterval() throws InputMismatchException {

        double startNumber, endNumber;
        double[][] interval = new double[8][2];
        Scanner scan = new Scanner(System.in);

        for (int i = 0; i < 8; i++) {
            System.out.print("Enter begin of interval:");
            startNumber = scan.nextDouble();

            System.out.print("Enter end of interval:");
            endNumber = scan.nextDouble();

            if (startNumber > endNumber) {
                throw new NumberFormatException();
            }
            if (i == 4) {
                System.out.println("Enter a, b, c intervals:");
            }
            interval[i][0] = startNumber;
            interval[i][1] = endNumber;
        }

        return interval;
    }

    //methods for activity on interval
}
